/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          February 7th, 2019                  */
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */


/* Preprocessor Guard                                       */
#ifndef STD_TYPES_H_
#define STD_TYPES_H_

#define STD_u8_ERROR 			(u8)1
#define STD_u8_OK				(u8)0


#define NULL 				((void *)0)

typedef unsigned char 		u8;
typedef unsigned short int 	u16;
typedef unsigned long int	u32;

typedef signed char 		s8;
typedef signed short int 	s16;
typedef signed long int		s32;

typedef float 				f32;
typedef double				f64;



/* Declaring Register type as a Union */
/* The Union can be configured using 2 different configurations: */
/* BitAccess: 	you can access every bit */
/* ByteAccess:	you write to the whole register */

typedef union
{
	struct
	{
		u8 Bit0 : 1;
		u8 Bit1 : 1;
		u8 Bit2 : 1;
		u8 Bit3 : 1;
		u8 Bit4 : 1;
		u8 Bit5 : 1;
		u8 Bit6 : 1;
		u8 Bit7 : 1;
	}BitAccess;

	u8 ByteAccess;
}Register_u8;


typedef union
{
	struct
	{
		u8 Bit0 	: 1;
		u8 Bit1 	: 1;
		u8 Bit2		: 1;
		u8 Bit3 	: 1;
		u8 Bit4 	: 1;
		u8 Bit5 	: 1;
		u8 Bit6 	: 1;
		u8 Bit7 	: 1;
		u8 Bit8 	: 1;
		u8 Bit9 	: 1;
		u8 Bit10 	: 1;
		u8 Bit11 	: 1;
		u8 Bit12 	: 1;
		u8 Bit13 	: 1;
		u8 Bit14 	: 1;
		u8 Bit15 	: 1;
		u8 Bit16 	: 1;
		u8 Bit17 	: 1;
		u8 Bit18 	: 1;
		u8 Bit19 	: 1;
		u8 Bit20 	: 1;
		u8 Bit21 	: 1;
		u8 Bit22 	: 1;
		u8 Bit23 	: 1;
		u8 Bit24 	: 1;
		u8 Bit25 	: 1;
		u8 Bit26 	: 1;
		u8 Bit27 	: 1;
		u8 Bit28 	: 1;
		u8 Bit29 	: 1;
		u8 Bit30 	: 1;
		u8 Bit31 	: 1;
	}BitAccess;

	u32 ByteAccess;
}Register_u32;

typedef void (*Callback_t)(void);

#endif
