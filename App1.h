/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 22th, 2019                	*/
/*	 Layer	 :			Application							*/
/*   Module  :			App1								*/
/*   Version :          V01	                                */
/*	 File	 :			Header file							*/
/*                                                          */
/* ******************************************************** */

#ifndef APP1_H_
#define APP1_H_


void App1_voidInit(void);
void App1_function(void);



#endif /* APP1_H_ */
