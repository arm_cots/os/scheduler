/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          February 27th, 2019                 */
/*   Version :          V01		                            */
/*                                                          */
/* ******************************************************** */

#include "STD_TYPES.h"
#include "BIT_CALC.h"

#include "DIO_interface.h"
#include "DIO_config.h"
#include "DIO_private.h"



void DIO_voidInit(void)
{
	DIO_u8SetPinDirection(DIO_u8_PIN0, DIO_u8_PIN0_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN1, DIO_u8_PIN1_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN2, DIO_u8_PIN2_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN3, DIO_u8_PIN3_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN4, DIO_u8_PIN4_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN5, DIO_u8_PIN5_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN6, DIO_u8_PIN6_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN7, DIO_u8_PIN7_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN8, DIO_u8_PIN8_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN9, DIO_u8_PIN9_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN10, DIO_u8_PIN10_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN11, DIO_u8_PIN11_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN12, DIO_u8_PIN12_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN13, DIO_u8_PIN13_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN14, DIO_u8_PIN14_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN15, DIO_u8_PIN15_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN16, DIO_u8_PIN16_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN17, DIO_u8_PIN17_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN18, DIO_u8_PIN18_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN19, DIO_u8_PIN19_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN20, DIO_u8_PIN20_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN21, DIO_u8_PIN21_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN22, DIO_u8_PIN22_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN23, DIO_u8_PIN23_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN24, DIO_u8_PIN24_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN25, DIO_u8_PIN25_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN26, DIO_u8_PIN26_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN27, DIO_u8_PIN27_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN28, DIO_u8_PIN28_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN29, DIO_u8_PIN29_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN30, DIO_u8_PIN30_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN31, DIO_u8_PIN31_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN32, DIO_u8_PIN32_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN33, DIO_u8_PIN33_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN34, DIO_u8_PIN34_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN35, DIO_u8_PIN35_INIT_DIR);
	DIO_u8SetPinDirection(DIO_u8_PIN36, DIO_u8_PIN36_INIT_DIR);



	DIO_u8SetPinValue(DIO_u8_PIN0, DIO_u8_PIN0_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN1, DIO_u8_PIN1_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN2, DIO_u8_PIN2_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN3, DIO_u8_PIN3_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN4, DIO_u8_PIN4_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN5, DIO_u8_PIN5_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN6, DIO_u8_PIN6_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN7, DIO_u8_PIN7_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN8, DIO_u8_PIN8_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN9, DIO_u8_PIN9_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN10, DIO_u8_PIN10_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN11, DIO_u8_PIN11_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN12, DIO_u8_PIN12_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN13, DIO_u8_PIN13_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN14, DIO_u8_PIN14_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN15, DIO_u8_PIN15_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN16, DIO_u8_PIN16_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN17, DIO_u8_PIN17_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN18, DIO_u8_PIN18_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN19, DIO_u8_PIN19_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN20, DIO_u8_PIN20_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN21, DIO_u8_PIN21_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN22, DIO_u8_PIN22_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN23, DIO_u8_PIN23_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN24, DIO_u8_PIN24_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN25, DIO_u8_PIN25_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN26, DIO_u8_PIN26_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN27, DIO_u8_PIN27_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN28, DIO_u8_PIN28_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN29, DIO_u8_PIN29_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN30, DIO_u8_PIN30_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN31, DIO_u8_PIN31_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN32, DIO_u8_PIN32_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN33, DIO_u8_PIN33_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN34, DIO_u8_PIN34_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN35, DIO_u8_PIN35_INIT_VAL);
	DIO_u8SetPinValue(DIO_u8_PIN36, DIO_u8_PIN36_INIT_VAL);

}





u8 DIO_u8SetPinDirection(u8 Copy_u8PinNum, u8 Copy_u8PinDirection)
{
	u8 Local_u8Error;


	if(Copy_u8PinNum < DIO_u8_MAX_PIN_NUM)
	{
		if( (Copy_u8PinNum <= DIO_u8_PIN7) )
		{
			ASG_NIBBLE(Copy_u8PinNum, Copy_u8PinDirection, DIO_u32_DIOA_CRL);
		}
		else if( (Copy_u8PinNum >= DIO_u8_PIN8) &&
				 (Copy_u8PinNum <= DIO_u8_PIN15) )
		{
			ASG_NIBBLE(Copy_u8PinNum-EIGHT, Copy_u8PinDirection, DIO_u32_DIOA_CRH);
		}
		else if( (Copy_u8PinNum >= DIO_u8_PIN16) &&
				 (Copy_u8PinNum <= DIO_u8_PIN23) )
		{
			ASG_NIBBLE(Copy_u8PinNum-SIXTEEN, Copy_u8PinDirection, DIO_u32_DIOB_CRL);
		}
		else if( (Copy_u8PinNum >= DIO_u8_PIN24) &&
				 (Copy_u8PinNum <= DIO_u8_PIN31) )
		{
			ASG_NIBBLE(Copy_u8PinNum-TWENTY_FOUR, Copy_u8PinDirection, DIO_u32_DIOB_CRH);
		}
		else if( (Copy_u8PinNum >= DIO_u8_PIN32) &&
				 (Copy_u8PinNum <= DIO_u8_PIN34) )
		{
			ASG_NIBBLE(Copy_u8PinNum-FOURTY, Copy_u8PinDirection, DIO_u32_DIOC_CRH);
		}
		else if( (Copy_u8PinNum >= DIO_u8_PIN35) &&
				 (Copy_u8PinNum <= DIO_u8_PIN36) )
		{
			ASG_NIBBLE(Copy_u8PinNum-FOURTY_EIGHT, Copy_u8PinDirection, DIO_u32_DIOD_CRL);
		}

	}
	else
	{
		Local_u8Error = STD_u8_ERROR;
	}

	return Local_u8Error;
}







u8 DIO_u8SetPinValue(u8 Copy_u8PinNum, u8 Copy_u8PinValue)
{
	u8 Local_u8Error;

	if( Copy_u8PinNum < DIO_u8_MAX_PIN_NUM)
	{
		switch(Copy_u8PinValue)
		{
			case DIO_u8_PIN_LOW:

				if( (Copy_u8PinNum <= DIO_u8_PIN15) )
				{
					CLR_BIT(DIO_u32_DIOA_ODR, Copy_u8PinNum);
				}
				else if( (Copy_u8PinNum >= DIO_u8_PIN16) &&
						 (Copy_u8PinNum <= DIO_u8_PIN31) )
				{
					CLR_BIT(DIO_u32_DIOB_ODR, Copy_u8PinNum-SIXTEEN);
				}
				else if( (Copy_u8PinNum >= DIO_u8_PIN32) &&
						 (Copy_u8PinNum <= DIO_u8_PIN34) )
				{
					CLR_BIT(DIO_u32_DIOC_ODR, Copy_u8PinNum-THIRTY_TWO);
				}
				Local_u8Error = STD_u8_OK;

				break;

			case DIO_u8_PIN_HIGH:

				if( (Copy_u8PinNum <= DIO_u8_PIN15) )
				{
					//DIO_u32_DIOA_ODR = 0x00000003;
					SET_BIT(DIO_u32_DIOA_ODR, Copy_u8PinNum);
				}
				else if( (Copy_u8PinNum >= DIO_u8_PIN16) &&
						 (Copy_u8PinNum <= DIO_u8_PIN31) )
				{
					SET_BIT(DIO_u32_DIOB_ODR, Copy_u8PinNum-SIXTEEN);
				}
				else if( (Copy_u8PinNum >= DIO_u8_PIN32) &&
						 (Copy_u8PinNum <= DIO_u8_PIN34) )
				{
					SET_BIT(DIO_u32_DIOC_ODR, Copy_u8PinNum-THIRTY_TWO);
				}
				else if( (Copy_u8PinNum >= DIO_u8_PIN35) &&
						 (Copy_u8PinNum <= DIO_u8_PIN36) )
				{
					SET_BIT(DIO_u32_DIOD_ODR, Copy_u8PinNum-FOURTY_EIGHT);
				}
				Local_u8Error = STD_u8_OK;

				break;

			default:
				Local_u8Error = STD_u8_ERROR;

				break;
		}
	}
	else
	{
		Local_u8Error = STD_u8_ERROR;
	}

	return Local_u8Error;

}


u8 DIO_u8GetPinValue(u8 Copy_u8PinNum, u8 *Copy_Pu8PinValueAddress)
{
	u8 Local_u8Error;

	if( (Copy_u8PinNum < DIO_u8_MAX_PIN_NUM) &&
		(Copy_Pu8PinValueAddress != NULL))
	{
		if( Copy_u8PinNum <= DIO_u8_PIN15 )
		{
			*Copy_Pu8PinValueAddress = GET_BIT(DIO_u32_DIOA_IDR,Copy_u8PinNum);
		}
		else if((Copy_u8PinNum >= DIO_u8_PIN16) &&
				(Copy_u8PinNum <= DIO_u8_PIN31))
		{
			*Copy_Pu8PinValueAddress = GET_BIT(DIO_u32_DIOB_IDR,Copy_u8PinNum-SIXTEEN);
		}
		else if( (Copy_u8PinNum >= DIO_u8_PIN32) &&
				 (Copy_u8PinNum <= DIO_u8_PIN34) )
		{
			*Copy_Pu8PinValueAddress = GET_BIT(DIO_u32_DIOA_IDR,Copy_u8PinNum-THIRTY_TWO);
		}
		else if( (Copy_u8PinNum >= DIO_u8_PIN35) &&
				 (Copy_u8PinNum <= DIO_u8_PIN36) )
		{
			*Copy_Pu8PinValueAddress = GET_BIT(DIO_u32_DIOA_IDR,Copy_u8PinNum-FOURTY_EIGHT);
		}

		Local_u8Error = STD_u8_OK;
	}
	else
	{
		Local_u8Error = STD_u8_ERROR;
	}

	return Local_u8Error;

}




u8 DIO_u8SetPortValue(u8 Copy_u8PortNum, u32 Copy_u32PortValue)
{
	u8 Local_u8Error;

	if(Copy_u8PortNum < DIO_u8_MAX_PORT_NUM)
	{
		switch(Copy_u8PortNum)
		{
			case DIO_u8_PORT0:
				ASG_WORD(DIO_u32_DIOA_ODR,Copy_u32PortValue);
				break;

			case DIO_u8_PORT1:
				ASG_WORD(DIO_u32_DIOB_ODR,Copy_u32PortValue);
				break;

			case DIO_u8_PORT2:
				ASG_WORD(DIO_u32_DIOC_ODR,Copy_u32PortValue);
				break;

			case DIO_u8_PORT3:
				ASG_WORD(DIO_u32_DIOD_ODR,Copy_u32PortValue);
				break;

		}
		Local_u8Error = STD_u8_OK;
	}
	else
	{
		Local_u8Error = STD_u8_ERROR;
	}


	return Local_u8Error;
}
