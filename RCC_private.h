/*
 * RCC_private.h
 *
 *  Created on: Feb 24, 2019
 *      Author: Sector Computer
 */

#ifndef RCC_PRIVATE_H_
#define RCC_PRIVATE_H_


#define RCC_u32_BASE_ADDRESS_VALUE			(u32)(0x40021000)

#define RCC_u32_CR_OFFSET_VALUE				(u32)(0x00)
#define RCC_u32_CFGR_OFFSET_VALUE 	        (u32)(0x04)
#define RCC_u32_CIR_OFFSET_VALUE 	        (u32)(0x08)
#define RCC_u32_APB2RSTR_OFFSET_VALUE       (u32)(0x0C)
#define RCC_u32_APB1RSTR_OFFSET_VALUE       (u32)(0x10)
#define RCC_u32_AHBENR_OFFSET_VALUE 	    (u32)(0x14)
#define RCC_u32_APB2ENR_OFFSET_VALUE        (u32)(0x18)
#define RCC_u32_APB1ENR_OFFSET_VALUE        (u32)(0x1C)
#define RCC_u32_BDCR_OFFSET_VALUE 	        (u32)(0x20)
#define RCC_u32_CSR_OFFSET_VALUE 	        (u32)(0x24)
#define RCC_u32_AHBRSTR_OFFSET_VALUE        (u32)(0x28)
#define RCC_u32_CFGR2_OFFSET_VALUE 	        (u32)(0x2C)




#define RCC_u32_CR 					((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_CR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_CFGR 				((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_CFGR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_CIR 				((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_CIR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_APB2RSTR 			((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_APB2RSTR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_APB1RSTR			((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_APB1RSTR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_AHBENR 				((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_AHBENR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_APB2ENR 			((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_APB2ENR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_APB1ENR 			((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_APB1ENR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_BDCR 				((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_BDCR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_CSR 				((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_CSR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_AHBRSTR 			((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_AHBRSTR_OFFSET_VALUE))->ByteAccess
#define RCC_u32_CFGR2 				((Register_u32*) (RCC_u32_BASE_ADDRESS_VALUE + RCC_u32_CFGR2_OFFSET_VALUE))->ByteAccess









#endif /* RCC_PRIVATE_H_ */
