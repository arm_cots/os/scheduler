/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019	                	*/
/*	 Layer	 :			Vertical Layer						*/
/*   Module  :			OS									*/
/*   Version :          V03	                                */
/*	 File	 :			prog.c								*/
/*                                                          */
/* ******************************************************** */

#include "BIT_CALC.h"
#include "STD_TYPES.h"

#include "SysTick_interface.h"
#include "OS_interface.h"

u8 OS_u8Flag;


void OS_voidInit(void)
{
	/*	Configuring the time after which the SysTick Timer generates an interrupt	*/
	SysTick_u8SetTimeMilliSecond(OS_TICK_TIME);
	/*	Setting the callback of the SysTick Timer, which is the Scheduler	*/
	SysTick_u8SetCallback(OS_voidSetOSFlag);

}


void OS_voidStart(void)
{
	/*	Starting the OS by enabling the SysTick timer		*/
	SysTick_u8Enable();
}

void OS_voidSetOSFlag(void)
{
	if(OS_u8Flag == 0)
	{
		/*	Setting the OS flag to make the scheduler run	*/
		OS_u8Flag = 1;
	}
}
void OS_voidScheduler(void)
{
	u8 Local_u8LoopIndex;
	/*	Checking the OS flag. If the flag is set, the scheduler would run		*/
	if(OS_u8Flag == 1)
	{
		for (Local_u8LoopIndex = 0; Local_u8LoopIndex < OS_SCHEDULER_CAPACITY; Local_u8LoopIndex++)
		{
			/*	Checking if the task exists										*/
			if(OS_ATasksArray[Local_u8LoopIndex].TaskPtr != NULL)
			{
				/*	Checking if the task is still active and not deleted			*/
				if(OS_ATasksArray[Local_u8LoopIndex].Status == OS_TASK_ACTIVE)
				{
					/*	Calling the runnables of the tasks based on their periodicity	*/
					if((OS_ATasksArray[Local_u8LoopIndex].RemainingTimeToExecute) == 0)
					{
						/*	This task will execute now so it shall start a new period	*/
						OS_ATasksArray[Local_u8LoopIndex].RemainingTimeToExecute = (u16)((OS_ATasksArray[Local_u8LoopIndex].TaskPtr->PeriodicityMilliSec)/OS_TICK_TIME);
						/*	Calling the runnable of the task	*/
						OS_ATasksArray[Local_u8LoopIndex].TaskPtr->Runnable();
					}
					(OS_ATasksArray[Local_u8LoopIndex].RemainingTimeToExecute)--;
				}
			}
		}
	}
	return;

}

void OS_voidMainFunction(void)
{
	while(1)
	{
		/*	This function calls the scheduler	*/
		if(OS_u8Flag == 1)
		{
			OS_voidScheduler();
			OS_u8Flag = 0;
		}

	}
}

u8 OS_u8CreateTask(const Task_t* Copy_PNewTask)
{
	u8 Local_u8Error = STD_u8_ERROR;

	if(Copy_PNewTask != NULL)
	{
		/*	Checking if there is a task already occupying this place in the array	*/
		if(OS_ATasksArray[Copy_PNewTask->Priority].TaskPtr == NULL)
		{
			/*	Storing the address of the task in the array of pointers to tasks	*/
			OS_ATasksArray[Copy_PNewTask->Priority].TaskPtr = Copy_PNewTask;
			/*	Updating the offset of the task										*/
			OS_ATasksArray[Copy_PNewTask->Priority].RemainingTimeToExecute = (u16)((OS_ATasksArray[Copy_PNewTask->Priority].TaskPtr->OffsetMilliSec)/OS_TICK_TIME);
			/*	Task status is active to make the task run							*/
			OS_ATasksArray[Copy_PNewTask->Priority].Status = OS_TASK_ACTIVE;
			Local_u8Error = STD_u8_OK;
		}
	}
	return Local_u8Error;
}


u8 OS_u8PauseTask(const Task_t* Copy_PNewTask, u8 Copy_u8PausingTicks)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	The priority parameter here is used as an index in the array of tasks	*/
	if(Copy_PNewTask->Priority < OS_SCHEDULER_CAPACITY)
	{
		/*	Checking if there's a task already registered in this index		*/
		if(OS_ATasksArray[Copy_PNewTask->Priority].TaskPtr != NULL)
		{
			/*	Pausing the task for a certain time							*/
			OS_ATasksArray[Copy_PNewTask->Priority].RemainingTimeToExecute = (u16)(Copy_u8PausingTicks * ((OS_ATasksArray[Copy_PNewTask->Priority].TaskPtr->PeriodicityMilliSec)/OS_TICK_TIME));
		}
	}
	return Local_u8Error;
}


u8 OS_u8DeleteTask(const Task_t* Copy_PNewTask)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	The priority parameter here is used as an index in the array of tasks	*/
	if(Copy_PNewTask->Priority < OS_SCHEDULER_CAPACITY)
	{
		/*	Checking if there's a task already registered in this index		*/
		if(OS_ATasksArray[Copy_PNewTask->Priority].TaskPtr != NULL)
		{
			/*	Deleting the task by changing its status					*/
			OS_ATasksArray[Copy_PNewTask->Priority].Status = OS_TASK_DELETED;
		}
	}

	return Local_u8Error;
}
