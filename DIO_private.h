/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          February 27th, 2019                 */
/*   Version :          V01		                            */
/*                                                          */
/* ******************************************************** */



#ifndef DIO_PRIVATE_H_
#define DIO_PRIVATE_H_


#define EIGHT								8
#define SIXTEEN								16
#define TWENTY_FOUR							24
#define THIRTY_TWO							32
#define FOURTY								40
#define FOURTY_EIGHT						48



#define DIO_u8_MAX_PIN_NUM 				(u8)48

#define DIO_u8_MAX_PORT_NUM				(u8)4




#define DIO_u32_PORTA_BASE_ADDRESS_VALUE			(u32) (0x40010800)
#define DIO_u32_PORTB_BASE_ADDRESS_VALUE			(u32) (0x40010C00)
#define DIO_u32_PORTC_BASE_ADDRESS_VALUE			(u32) (0x40011000)
#define DIO_u32_PORTD_BASE_ADDRESS_VALUE			(u32) (0x40011400)


#define DIO_u32_CRL_OFFSET_VALUE					(u32) (0x00)
#define DIO_u32_CRH_OFFSET_VALUE					(u32) (0x04)
#define DIO_u32_IDR_OFFSET_VALUE					(u32) (0x08)
#define DIO_u32_ODR_OFFSET_VALUE					(u32) (0x0C)
#define DIO_u32_BSRR_OFFSET_VALUE					(u32) (0x10)
#define DIO_u32_BRR_OFFSET_VALUE					(u32) (0x14)
#define DIO_u32_LCKR_OFFSET_VALUE					(u32) (0x18)


#define DIO_u32_DIOA_CRL							((Register_u32*) (DIO_u32_PORTA_BASE_ADDRESS_VALUE + DIO_u32_CRL_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOA_CRH							((Register_u32*) (DIO_u32_PORTA_BASE_ADDRESS_VALUE + DIO_u32_CRH_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOA_IDR							((Register_u32*) (DIO_u32_PORTA_BASE_ADDRESS_VALUE + DIO_u32_IDR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOA_ODR							((Register_u32*) (DIO_u32_PORTA_BASE_ADDRESS_VALUE + DIO_u32_ODR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOA_BSRR							((Register_u32*) (DIO_u32_PORTA_BASE_ADDRESS_VALUE + DIO_u32_BSRR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOA_BRR							((Register_u32*) (DIO_u32_PORTA_BASE_ADDRESS_VALUE + DIO_u32_BRR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOA_LCKR							((Register_u32*) (DIO_u32_PORTA_BASE_ADDRESS_VALUE + DIO_u32_LCKR_OFFSET_VALUE) )->ByteAccess

#define DIO_u32_DIOB_CRL							((Register_u32*) (DIO_u32_PORTB_BASE_ADDRESS_VALUE + DIO_u32_CRL_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOB_CRH							((Register_u32*) (DIO_u32_PORTB_BASE_ADDRESS_VALUE + DIO_u32_CRH_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOB_IDR							((Register_u32*) (DIO_u32_PORTB_BASE_ADDRESS_VALUE + DIO_u32_IDR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOB_ODR							((Register_u32*) (DIO_u32_PORTB_BASE_ADDRESS_VALUE + DIO_u32_ODR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOB_BSRR							((Register_u32*) (DIO_u32_PORTB_BASE_ADDRESS_VALUE + DIO_u32_BSRR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOB_BRR							((Register_u32*) (DIO_u32_PORTB_BASE_ADDRESS_VALUE + DIO_u32_BRR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOB_LCKR							((Register_u32*) (DIO_u32_PORTB_BASE_ADDRESS_VALUE + DIO_u32_LCKR_OFFSET_VALUE) )->ByteAccess

#define DIO_u32_DIOC_CRL							((Register_u32*) (DIO_u32_PORTC_BASE_ADDRESS_VALUE + DIO_u32_CRL_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOC_CRH							((Register_u32*) (DIO_u32_PORTC_BASE_ADDRESS_VALUE + DIO_u32_CRH_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOC_IDR							((Register_u32*) (DIO_u32_PORTC_BASE_ADDRESS_VALUE + DIO_u32_IDR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOC_ODR							((Register_u32*) (DIO_u32_PORTC_BASE_ADDRESS_VALUE + DIO_u32_ODR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOC_BSRR							((Register_u32*) (DIO_u32_PORTC_BASE_ADDRESS_VALUE + DIO_u32_BSRR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOC_BRR							((Register_u32*) (DIO_u32_PORTC_BASE_ADDRESS_VALUE + DIO_u32_BRR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOC_LCKR							((Register_u32*) (DIO_u32_PORTC_BASE_ADDRESS_VALUE + DIO_u32_LCKR_OFFSET_VALUE) )->ByteAccess


#define DIO_u32_DIOD_CRL							((Register_u32*) (DIO_u32_PORTD_BASE_ADDRESS_VALUE + DIO_u32_CRL_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOD_CRH							((Register_u32*) (DIO_u32_PORTD_BASE_ADDRESS_VALUE + DIO_u32_CRH_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOD_IDR							((Register_u32*) (DIO_u32_PORTD_BASE_ADDRESS_VALUE + DIO_u32_IDR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOD_ODR							((Register_u32*) (DIO_u32_PORTD_BASE_ADDRESS_VALUE + DIO_u32_ODR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOD_BSRR							((Register_u32*) (DIO_u32_PORTD_BASE_ADDRESS_VALUE + DIO_u32_BSRR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOD_BRR							((Register_u32*) (DIO_u32_PORTD_BASE_ADDRESS_VALUE + DIO_u32_BRR_OFFSET_VALUE) )->ByteAccess
#define DIO_u32_DIOD_LCKR							((Register_u32*) (DIO_u32_PORTD_BASE_ADDRESS_VALUE + DIO_u32_LCKR_OFFSET_VALUE) )->ByteAccess




#endif /* DIO_PRIVATE_H_ */
