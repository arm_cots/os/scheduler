/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019	                	*/
/*   Module  :			SysTick								*/
/*   Version :          V03	                                */
/*	 File	 :			main.c								*/
/*                                                          */
/* ******************************************************** */


#include "BIT_CALC.h"
#include "STD_TYPES.h"

#include "RCC_interface.h"
#include "DIO_interface.h"
#include "SysTick_interface.h"
#include "OS_interface.h"
#include "App1.h"
#include "App2.h"


void main(void)
{
	RCC_voidInit();
	RCC_voidSetPeripheralState(RCC_u32_IOPA_NUMBER, RCC_u8_PERI_STATE_ENABLED);

	DIO_voidInit();
	SysTick_voidInit();
	App1_voidInit();
	App2_voidInit();
	OS_voidInit();
	OS_voidStart();
	OS_voidMainFunction();

	while(1)
	{

	}




	return;
}


