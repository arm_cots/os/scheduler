/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 22th, 2019                	*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			SysTick								*/
/*   Version :          V02	                                */
/*	 File	 :			prog.c								*/
/*                                                          */
/* ******************************************************** */

#include "BIT_CALC.h"
#include "STD_TYPES.h"

#include "SysTick_interface.h"
#include "SysTick_private.h"
#include "SysTick_config.h"


Callback_t SysTick_CallbackFunctionPtr;


void SysTick_voidInit()
{

	/*	Configuring the SysTick Timer clock source		*/
	SYSTICK_u32_STK_CTRL = SysTickConfigurator.ClockSource;

	/*	Enabling the SysTick Timer interrupt			*/
	SYSTICK_u32_STK_CTRL |= SysTickConfigurator.InterruptEnable;

	/*	Initializing the callback function by NULL		*/
	SysTick_CallbackFunctionPtr = NULL;

	return;
}

u8 SysTick_u8Enable()
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Checking if the SysTick timer is disabled		*/
	if(GET_BIT(SYSTICK_u32_STK_CTRL, SYSTICK_STK_CTRL_ENABLE_PIN) == 0)
	{
		/*	Enabling the SysTick timer 					*/
		SET_BIT(SYSTICK_u32_STK_CTRL, SYSTICK_STK_CTRL_ENABLE_PIN);
		Local_u8Error = STD_u8_OK;
	}
	return Local_u8Error;
}

u8 SysTick_u8Disable()
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Checking if the SysTick timer is enabled		*/
	if(GET_BIT(SYSTICK_u32_STK_CTRL, SYSTICK_STK_CTRL_ENABLE_PIN) == 1)
	{
		/*	Disabling the SysTick timer 				*/
		CLR_BIT(SYSTICK_u32_STK_CTRL, SYSTICK_STK_CTRL_ENABLE_PIN);
		Local_u8Error = STD_u8_OK;
	}
	return Local_u8Error;
}



u8 SysTick_u8SetTimeMilliSecond(u16 Copy_u16DesiredTimeMilliSecond)
{
	u8 Local_u8Error = STD_u8_ERROR;
	f64 Local_f64TickTime = 1;
	u32 Local_u32ReloadValue;

	/*	Calculating the tick time based on the clock source		*/
	if(SysTickConfigurator.ClockSource == SYSTICK_u8_CLK_SRC_AHB)
	{
		Local_f64TickTime = 1.0 / (AHB_CLOCK);
		Local_u8Error = STD_u8_OK;
	}
	else if(SysTickConfigurator.ClockSource == SYSTICK_u8_CLK_SRC_AHB_DIVIDED_BY_8)
	{
		Local_f64TickTime = 1.0 / (AHB_CLOCK/8);
		Local_u8Error = STD_u8_OK;
	}

	/*	Setting the load value									*/
	Local_u32ReloadValue = ((Copy_u16DesiredTimeMilliSecond) / Local_f64TickTime) * 0.001;
	SYSTICK_u32_STK_LOAD = Local_u32ReloadValue;

	return Local_u8Error;
}

u8 SysTick_u8SetTimeMicroSecond(u16 Copy_u16DesiredTimeMicroSecond)
{
	u8 Local_u8Error = STD_u8_ERROR;
	f64 Local_f64TickTime = 1;
	u32 Local_u32ReloadValue;

	/*	Calculating the tick time based on the clock source		*/
	if(SysTickConfigurator.ClockSource == SYSTICK_u8_CLK_SRC_AHB)
	{
		Local_f64TickTime = 1.0 / (AHB_CLOCK);
		Local_u8Error = STD_u8_OK;
	}
	else if(SysTickConfigurator.ClockSource == SYSTICK_u8_CLK_SRC_AHB_DIVIDED_BY_8)
	{
		Local_f64TickTime = 1.0 / (AHB_CLOCK/8);
		Local_u8Error = STD_u8_OK;
	}

	/*	Setting the load value									*/
	Local_u32ReloadValue = ((Copy_u16DesiredTimeMicroSecond * 0.000001) / Local_f64TickTime);
	SYSTICK_u32_STK_LOAD = Local_u32ReloadValue;

	return Local_u8Error;
}

u8 SysTick_u8SetCallback(Callback_t Copy_PvoidCallbackFunctionPtr)
{
	u8 Local_u8Error = STD_u8_ERROR;

	/*	Setting the callback function 							*/
	if(Copy_PvoidCallbackFunctionPtr != NULL)
	{
		SysTick_CallbackFunctionPtr = Copy_PvoidCallbackFunctionPtr;
	}
	return Local_u8Error;
}



void SysTick_Handler(void)
{
	if(SysTick_CallbackFunctionPtr != NULL)
	{
		SysTick_CallbackFunctionPtr();
	}

	return;
}
