/*
 * RCC_interface.h
 *
 *  Created on: Feb 24, 2019
 *      Author: Sector Computer
 */

#ifndef RCC_INTERFACE_H_
#define RCC_INTERFACE_H_

#define RCC_u32_IOPA_NUMBER			(u32)2
#define RCC_u32_IOPB_NUMBER			(u32)3
#define RCC_u32_IOPC_NUMBER			(u32)4
#define RCC_u32_IOPD_NUMBER			(u32)5

#define RCC_u8_PERI_STATE_ENABLED	(u8) 40
#define RCC_u8_PERI_STATE_DISABLED	(u8) 50



void RCC_voidInit(void);

void RCC_voidSetPeripheralState(u8 Copy_u8PeripheralNo, u8 Copy_u8PeripheralState);

#endif /* RCC_INTERFACE_H_ */
