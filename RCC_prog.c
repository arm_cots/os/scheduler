/*
 * RCC_prog.c
 *
 *  Created on: Feb 24, 2019
 *      Author: Sector Computer
 */

#include "STD_TYPES.h"
#include "BIT_CALC.h"

#include "RCC_interface.h"
#include "RCC_private.h"
#include "RCC_config.h"


void RCC_voidInit(void)
{
	// Enabling the HSI clock
	RCC_u32_CR = 0x00000001;

	// Checking if the HSI clock is ready
	//while( (RCC_u32_CR & 0x00000002) == 0);


	// No prescaler for AHB, APB1, and APB2
	// And selecting HSI as the SYSCLK
	RCC_u32_CFGR = 0x00000000;

}

void RCC_voidSetPeripheralState(u8 Copy_u8PeripheralNo, u8 Copy_u8PeripheralState)
{

	if(Copy_u8PeripheralState == RCC_u8_PERI_STATE_ENABLED)
	{
		RCC_u32_APB2ENR |= (1 << Copy_u8PeripheralNo);
	}
	else if(Copy_u8PeripheralState == RCC_u8_PERI_STATE_DISABLED)
	{
		RCC_u32_APB2ENR &= ~(1 << Copy_u8PeripheralNo);
	}

}
