/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          February 7th, 2019                  */
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */


/* Preprocessor Guard                                       */
#ifndef   DELAY_H
#define   DELAY_H

#define   INNER_LOOP_ITERATIONS       1000
 


 
void delay_milliseconds (u32 Copy_u32OuterLoopIterations)
{
  
	u32 u32OuterLoopIndex = 0, u32InnerLoopIndex = 0;

	for(u32OuterLoopIndex = 0; u32OuterLoopIndex < Copy_u32OuterLoopIterations; u32OuterLoopIndex++)
	{
    
		// This loop yields 1 millisecond
    
		for(u32InnerLoopIndex = 0; u32InnerLoopIndex < INNER_LOOP_ITERATIONS; u32InnerLoopIndex++)
		{
			asm("NOP");
			asm("NOP");
			asm("NOP");
			asm("NOP");
		}
	}

}


#endif
