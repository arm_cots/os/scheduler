/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019	                	*/
/*	 Layer	 :			Vertical Layer						*/
/*   Module  :			OS									*/
/*   Version :          V03	                                */
/*	 File	 :			interface.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef OS_INTERFACE_H_
#define OS_INTERFACE_H_

/*	The maximum number of tasks that the scheduler can handle	*/
#define OS_SCHEDULER_CAPACITY		3

/*	Tick time in milli seconds	*/
#define OS_TICK_TIME				(u16)500


/*	Task status					*/
#define OS_TASK_ACTIVE				(u8)1
#define OS_TASK_DELETED				(u8)2

typedef struct
{
	Callback_t	Runnable;
	u16			PeriodicityMilliSec;
	u16 		OffsetMilliSec;
	u8 			Priority;

}Task_t;

typedef struct
{
	const Task_t* 	TaskPtr;
	u16				RemainingTimeToExecute;
	u8				Status;
}OSTask_t;

/*	Creating an array of tasks	*/
OSTask_t OS_ATasksArray[OS_SCHEDULER_CAPACITY];

void OS_voidInit(void);
void OS_voidStart(void);
void OS_voidSetOSFlag(void);
void OS_voidScheduler(void);
void OS_voidMainFunction(void);
u8 OS_u8CreateTask(const Task_t* Copy_PNewTask);
u8 OS_u8PauseTask(const Task_t* Copy_PNewTask, u8 Copy_u8PausingTicks);
u8 OS_u8DeleteTask(const Task_t* Copy_PNewTask);

#endif /* OS_INTERFACE_H_ */
