/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019	                	*/
/*	 Layer	 :			Application							*/
/*   Module  :			App2								*/
/*   Version :          V03	                                */
/*	 File	 :			Source file							*/
/*                                                          */
/* ******************************************************** */

#include "BIT_CALC.h"
#include "STD_TYPES.h"

#include "DIO_interface.h"
#include "OS_interface.h"

#include "App2.h"

/*	Creating a dummy task to test the scheduler */
const Task_t T2 =
{
		App2_function,
		2000,
		0,
		2
};


void App2_voidInit(void)
{
	/*	Creating a Task by passing the address of T2 to the array	*/
	OS_u8CreateTask(&T2);
}

/*	Runnable of T1, this task toggles a LED	*/
void App2_function(void)
{
	static u8 value = 0;
	if(value == 0)
	{
		DIO_u8SetPinValue(1, DIO_u8_PIN_HIGH);
		value = 1;
	}
	else
	{
		DIO_u8SetPinValue(1, DIO_u8_PIN_LOW);
		value = 0;
	}
}
