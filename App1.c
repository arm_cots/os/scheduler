/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          May 20th, 2019    	            	*/
/*	 Layer	 :			Application							*/
/*   Module  :			App1								*/
/*   Version :          V03	                                */
/*	 File	 :			Source file							*/
/*                                                          */
/* ******************************************************** */

#include "BIT_CALC.h"
#include "STD_TYPES.h"

#include "DIO_interface.h"
#include "OS_interface.h"

#include "App1.h"


/*	Creating a dummy task to test the scheduler */
const Task_t T1 =
{
		App1_function,
		500,
		2000,
		0
};


void App1_voidInit(void)
{
	/*	Creating a Task by passing the address of T1 to the array	*/
	OS_u8CreateTask(&T1);
}

/*	Runnable of T1, this task toggles a LED	*/
void App1_function(void)
{
	static u8 value = 0;
	if(value == 0)
	{
		DIO_u8SetPinValue(0, DIO_u8_PIN_HIGH);
		value = 1;
	}
	else
	{
		DIO_u8SetPinValue(0, DIO_u8_PIN_LOW);
		value = 0;
	}
}
