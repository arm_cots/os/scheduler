/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 22th, 2019                	*/
/*	 Layer	 :			Application							*/
/*   Module  :			App2								*/
/*   Version :          V01	                                */
/*	 File	 :			Header file							*/
/*                                                          */
/* ******************************************************** */

#ifndef APP2_H_
#define APP2_H_


void App2_voidInit(void);
void App2_function(void);



#endif /* APP2_H_ */
