/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 22th, 2019                	*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			SysTick								*/
/*   Version :          V02	                                */
/*	 File	 :			config.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef SYSTICK_CONFIG_H_
#define SYSTICK_CONFIG_H_

#define AHB_CLOCK										(f64)8000000.0


SysTickConfiguration_t SysTickConfigurator = {

		SYSTICK_u8_INTERRUPT_ENABLE,
		SYSTICK_u8_CLK_SRC_AHB_DIVIDED_BY_8

};


#endif /* SYSTICK_CONFIG_H_ */
