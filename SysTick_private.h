/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 22th, 2019                	*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			SysTick								*/
/*   Version :          V02	                                */
/*	 File	 :			private.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef SYSTICK_PRIVATE_H_
#define SYSTICK_PRIVATE_H_


#define SYSTICK_u32_BASE_ADDRESS_VALUE			(u32)(0xE000E010)


#define SYSTICK_u32_CTRL_OFFSET_VALUE			(u32)(0x0000)
#define SYSTICK_u32_LOAD_OFFSET_VALUE			(u32)(0x0004)
#define SYSTICK_u32_VAL_OFFSET_VALUE			(u32)(0x0008)
#define SYSTICK_u32_CALIB_OFFSET_VALUE			(u32)(0x000C)


#define SYSTICK_u32_STK_CTRL					*((u32*)(SYSTICK_u32_BASE_ADDRESS_VALUE + SYSTICK_u32_CTRL_OFFSET_VALUE))
#define SYSTICK_u32_STK_LOAD					*((u32*)(SYSTICK_u32_BASE_ADDRESS_VALUE + SYSTICK_u32_LOAD_OFFSET_VALUE))
#define SYSTICK_u32_STK_VAL						*((u32*)(SYSTICK_u32_BASE_ADDRESS_VALUE + SYSTICK_u32_VAL_OFFSET_VALUE))
#define SYSTICK_u32_STK_CALIB					*((u32*)(SYSTICK_u32_BASE_ADDRESS_VALUE + SYSTICK_u32_CALIB_OFFSET_VALUE))



#define SYSTICK_STK_CTRL_ENABLE_PIN				0
#define SYSTICK_STK_CTRL_TICKINT_PIN			1
#define SYSTICK_STK_CTRL_CLKSOURCE_PIN			2
#define SYSTICK_STK_CTRL_COUNTFLAG_PIN			16



#define SYSTICK_u8_COUNTER_ENABLE				(u8)0x01
#define SYSTICK_u8_COUNTER_DISABLE				(u8)0x00
#define SYSTICK_u8_INTERRUPT_ENABLE				(u8)0x02
#define SYSTICK_u8_INTERRUPT_DISABLE			(u8)0x00
#define SYSTICK_u8_CLK_SRC_AHB					(u8)0x04
#define SYSTICK_u8_CLK_SRC_AHB_DIVIDED_BY_8		(u8)0x00


typedef struct
{
	u8 InterruptEnable;
	u8 ClockSource;

}SysTickConfiguration_t;



#endif /* SYSTICK_PRIVATE_H_ */
