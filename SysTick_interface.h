/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 22th, 2019                	*/
/*	 Layer	 :			MCAL								*/
/*   Module  :			SysTick								*/
/*   Version :          V02	                                */
/*	 File	 :			interface.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef SYSTICK_INTERFACE_H_
#define SYSTICK_INTERFACE_H_

void SysTick_voidInit();
u8 SysTick_u8Enable();
u8 SysTick_u8Disable();
u8 SysTick_u8SetTimeMilliSecond(u16 Copy_u16DesiredTimeMilliSecond);
u8 SysTick_u8SetTimeMicroSecond(u16 Copy_u16DesiredTimeMicroSecond);
u8 SysTick_u8SetCallback(Callback_t Copy_PvoidCallbackFunctionPtr);




#endif /* SYSTICK_INTERFACE_H_ */
